Ecume − Enregistreur CycliqUe MultipostE
========================================

Enregistre le flux de BFM TV sous la forme de deux fichiers par heure.

Prérequis
---------

* Freebox avec multiposte activé
* ffmpeg
* vlc

Utilisation
-----------

* Le flux HD n’est consultable qu’une fois la captation terminée (temps d’attente max : 1 heure).
* Le flux dégradé est dans un format consultable à n’importe quel moment.


