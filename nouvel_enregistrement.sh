#!/bin/sh

ANNEE=$(date +"%Y")
MOIS=$(date +"%m")
JOUR=$(date +"%d")
HEURE_DEBUT=$(date +"%H")
HEURE_FIN=$(($(date +"%H")+1))
MINUTE=$(date +"%M")
DEBUT=$HEURE_DEBUT:$MINUTE
FIN=$HEURE_FIN:$MINUTE
REP=$ANNEE/$MOIS/$JOUR/
TITRE_BRUT=$(curl -s www.bfmtv.com/mediaplayer/live-video/ | grep "twitter btn btn-default btn-sm" | sed -e 's/.\+data-text="\([^"]\+\).\+/\1/g')
TITRE=$(echo $TITRE_BRUT | tr -cd 'A-Za-z0-9_.-')
BASENAME=$DEBUT-$TITRE-$FIN

mkdir -p $REP
cd $REP
#mencoder -quiet -nocache "rtsp://mafreebox.freebox.fr/fbxtv_pub/stream?namespace=1&service=400&flavour=hd" -oac copy -ovc copy -endpos 01:00:00 -o $BASENAME-hq.ts &
#mencoder -quiet -nocache "rtsp://mafreebox.freebox.fr/fbxtv_pub/stream?namespace=1&service=400&flavour=ld" -oac copy -ovc copy -endpos 01:00:00 -o $BASENAME-bq.ts &
#cvlc  --play-and-exit --stop-time 10 "rtsp://mafreebox.freebox.fr/fbxtv_pub/stream?namespace=1&service=400&flavour=hd" --sout=file/ts:$BASENAME-hq.ts &
#cvlc  --play-and-exit --stop-time 10 "rtsp://mafreebox.freebox.fr/fbxtv_pub/stream?namespace=1&service=400&flavour=ld" --sout=file/ts:$BASENAME-bq.ts &
cvlc  --play-and-exit --stop-time 3600 "rtsp://mafreebox.freebox.fr/fbxtv_pub/stream?namespace=1&service=400&flavour=hd" --sout=file/mp4:$BASENAME-hq.mp4 &
#cvlc  --play-and-exit --stop-time 3600 "rtsp://mafreebox.freebox.fr/fbxtv_pub/stream?namespace=1&service=400&flavour=ld" --sout=file/mp4:$BASENAME-bq.mp4 &
#ffmpeg -v quiet -i "rtsp://mafreebox.freebox.fr/fbxtv_pub/stream?namespace=1&service=400&flavour=ld" -to 01:00:00 -preset ultrafast -vcodec libx264 -acodec libmp3lame -b:a 12k -b:v 333k -y $BASENAME-bq.mp4 &
#ffmpeg -nostdin -v quiet -i "rtsp://mafreebox.freebox.fr/fbxtv_pub/stream?namespace=1&service=400&flavour=ld" -to 01:00:00 -preset ultrafast -codec:v libx264 -codec:a libvorbis -qscale:a 0 -b:v 333k -y "file:$BASENAME-bq.mp4" &
ffmpeg -nostdin -v quiet  -i "rtsp://mafreebox.freebox.fr/fbxtv_pub/stream?namespace=1&service=400&flavour=ld" -to 01:00:00 -streamid 0:0 -streamid 2:1 -preset ultrafast -codec:v libx264 -codec:a libmp3lame -b:a 10k -r 10 -b:v 234k -bufsize 3000k  -f mpegts -y "file:$BASENAME-bq.ts" &



